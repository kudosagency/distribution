var stream = require('stream');
const puppeteer = require('puppeteer');
const select = require ('puppeteer-select');
const csv = require('@fast-csv/parse');
const { convertArrayToCSV } = require('convert-array-to-csv');
const converter = require('convert-array-to-csv');
const fs = require('fs');
require('dotenv').config()

exports.index = function(req, res) {
    res.render('index');
};

exports.file = function(req, res) {
    (async () => {
        const browser = await puppeteer.launch({headless: true, args:['--no-sandbox']})
        const page = await browser.newPage()
        await page.setDefaultNavigationTimeout(10000);
        await page.goto('https://ahrefs.com/user/login')
        await page.type('#email_input', process.env.AHREF_USERNAME)
        await page.type('[name="password"]', process.env.AHREF_PASSWORD)
        await page.click('#SignInButton')
        await page.waitForNavigation()
        await page.goto('https://ahrefs.com/keywords-explorer')
        await page.waitForSelector('.entryTextarea-input')
        await page.click('.entry-country button')
        await page.waitForSelector('.flag-icon-gb')
        await page.click('.flag-icon-gb')

        fs.readFile(req.file.path, "utf8", async (err, data) => {
          if (err) throw err;

          // keyword
          await page.type('.entryTextarea-input', data)
          await page.click('.entry-button button')

          await page.waitForSelector('.result-main')

          await select(page).assertElementPresent('a span:contains(Phrase match)');
          const element = await select(page).getElement('a span:contains(Phrase match)');
          await element.click()

          await page.waitForSelector('.result-main')

          let rows = await page.evaluate(() => {
              let results = [];
              results.push([
                  'Keyword',
                  'Volume',
                  'Results clicked %',
                  'Results not clicked %',
                  'Clicks',
                  'Organic clicks %',
                  'Paid Clicks %',
                  'Cost per click'
              ]);
              let items = document.querySelectorAll('.result-main');
              if(items.length) {
                  items.forEach((item) => {
                    let clicksChart = item.querySelector('.resultsTable-clicksChart');
                    let bars = clicksChart.querySelectorAll('.distribution-fill');

                    let volume = item.querySelector('.resultsTable-volume');
                    let clicks = item.querySelector('.resultsTable-clicks');
                    let cost = item.querySelector('.cpcCell');

                    let volumeChart = item.querySelector('.resultsTable-volumeChart');
                    let barsV = volumeChart.querySelectorAll('.distribution-fill');

                    let distribution = [
                        item.querySelector('.keywordCell').innerText,
                        'N/A',
                        'N/A',
                        'N/A',
                        'N/A',
                        'N/A',
                        'N/A',
                        'N/A'
                    ];
                    if (bars.length) {
                        distribution = [
                            item.querySelector('.keywordCell').innerText,
                            volume.innerText,
                            barsV[0].style.width,
                            barsV[1].style.width,
                            clicks.innerText,
                            bars[0].style.width,
                            bars[1].style.width,
                            cost.innerText
                        ];
                    }
                    results.push(distribution);
                  });
              }
              return results;
          })
          browser.close()
          const csvData = convertArrayToCSV(rows);
          const fileContents = Buffer.from(csvData);
          const readStream = new stream.PassThrough();
          readStream.end(fileContents.toString());
          res.set('Content-disposition', 'attachment; filename=export.csv');
          res.set('Content-Type', 'text/csv');
          readStream.pipe(res)
        })
      })()
};

exports.keyword = function(req, res) {
    (async () => {
        const browser = await puppeteer.launch({headless: true, args:['--no-sandbox']})
        const page = await browser.newPage()
        await page.setDefaultNavigationTimeout(10000);
        await page.goto('https://ahrefs.com/user/login')
        await page.type('#email_input', process.env.AHREF_USERNAME)
        await page.type('[name="password"]', process.env.AHREF_PASSWORD)
        await page.click('#SignInButton')
        await page.waitForNavigation()
        await page.goto('https://ahrefs.com/keywords-explorer')

        // keyword
        await page.waitForSelector('.entryTextarea-input')
        await page.type('.entryTextarea-input', req.body.keywords)
        await page.click('.entry-country button')
        await page.waitForSelector('.flag-icon-gb')
        await page.click('.flag-icon-gb')
        await page.click('.entry-button button')

        await select(page).assertElementPresent('a span:contains(Phrase match)');
        const element = await select(page).getElement('a span:contains(Phrase match)');
        await element.click()
        await page.waitForSelector('.result-main')

        let rows = await page.evaluate(() => {
            let results = [];
            results.push([
                'Keyword',
                'Volume',
                'Results clicked %',
                'Results not clicked %',
                'Clicks',
                'Organic clicks %',
                'Paid Clicks %',
                'Cost per click'
            ]);
            let items = document.querySelectorAll('.result-main');
            if(items.length) {
                items.forEach((item) => {
                  let clicksChart = item.querySelector('.resultsTable-clicksChart');
                  let bars = clicksChart.querySelectorAll('.distribution-fill');

                  let volume = item.querySelector('.resultsTable-volume');
                  let clicks = item.querySelector('.resultsTable-clicks');
                  let cost = item.querySelector('.cpcCell');

                  let volumeChart = item.querySelector('.resultsTable-volumeChart');
                  let barsV = volumeChart.querySelectorAll('.distribution-fill');
                  let distribution = [
                    item.querySelector('.keywordCell').innerText,
                    'N/A',
                    'N/A',
                    'N/A',
                    'N/A',
                    'N/A',
                    'N/A',
                    'N/A'
                  ];
                  if (bars.length) {
                      distribution = [
                          item.querySelector('.keywordCell').innerText,
                          volume.innerText,
                          barsV[0].style.width,
                          barsV[1].style.width,
                          clicks.innerText,
                          bars[0].style.width,
                          bars[1].style.width,
                          cost.innerText
                      ];
                  }
                  results.push(distribution);
                });
            }
            return results;
        })
        browser.close()
        const csvData = convertArrayToCSV(rows);
        const fileContents = Buffer.from(csvData);
        const readStream = new stream.PassThrough();
        readStream.end(fileContents.toString());
        res.set('Content-disposition', 'attachment; filename=export.csv');
        res.set('Content-Type', 'text/csv');
        readStream.pipe(res);
      })()
};
