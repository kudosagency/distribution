const multer  = require('multer')
const express = require('express')
const keywordController = require('../controllers/keywordController')

const upload = multer({
    dest: 'tmp/csv/',
    trim: true,
    ignoreEmpty: true
})
const router = express.Router()

/* home */
router.get('/', keywordController.index)

/* 2 types of results */
router.post('/file', upload.single('csv'), keywordController.file)
router.post('/keyword', keywordController.keyword)

module.exports = router
